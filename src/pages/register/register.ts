import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { User } from '../models/user';
import { LoginPage } from '../login/login';
import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  user = {} as User;

  constructor(private menuCtrl: MenuController, private aFAuth: AngularFireAuth, public navCtrl: NavController, public navParams: NavParams) {
    this.menuCtrl.enable(false);
  }

  login(){
    this.navCtrl.push(LoginPage);
  }

  async register(user: User){
    try {
      const result = await this.aFAuth.auth.createUserWithEmailAndPassword(user.email, user.password);
      if(result){
        this.navCtrl.push(LoginPage);
      }
    } catch (error) {
      console.error(error);
    }
  }

}
