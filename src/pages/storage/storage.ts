import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Record } from '../models/file';


@Component({
  selector: 'page-storage',
  templateUrl: 'storage.html',
})
export class StoragePage {

  files: Observable<{}[]>;

  record = {} as Record;

  constructor(private alertCtrl: AlertController, private social: SocialSharing, private aFAuth: AngularFireAuth, private afs: AngularFirestore, private menuCtrl: MenuController, public navCtrl: NavController, public navParams: NavParams) {
    this.menuCtrl.enable(true);
    this.files = this.afs.collection("records", ref => ref.where("uid", "==", this.aFAuth.auth.currentUser.uid)).valueChanges();
  }

  send(file){
    let message: string = "This is your conversion file for " + file.enzyme + " enzyme. You can use this file further in Excel.";
    let subject: string = "GlycoSpot Unit Conversion";
    let recipient: string[] = [this.aFAuth.auth.currentUser.email];
    this.social.shareViaEmail(message, subject, recipient, null, null, file.base64);
  }

  delete(file){
    let confirm = this.alertCtrl.create({
      title: 'Do you want to delete this file from database?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.deleteFile(file);
          }
        }
      ]
    });
    confirm.present();
  }

  deleteFile(file){
    let array = [];
    this.afs.collection("records").snapshotChanges().subscribe(res => {
      res.map(c => {
        console.log(c);
        console.log(typeof(c));
        array.push(c);
      })
    })
    console.log(array);
    this.afs.collection("records", ref => ref.where("uid", "==", file.uid).where("timestamp", "==", file.timestamp)).snapshotChanges().subscribe(files => {
      files.map(a => {
        this.afs.collection("records").doc(a.payload.doc.id).delete().then(_ => console.log("deleted")).catch(err => console.log(err));
      })
    })
    
  }
}
