import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from '../models/user';
import { ProfileEditPage } from '../profile-edit/profile-edit';
import { MenuController } from 'ionic-angular/components/app/menu-controller';



@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  user = {} as User;

  isPhotoAvailable = false;

  constructor(private menuCtrl: MenuController, private aFAuth: AngularFireAuth, public navCtrl: NavController, public navParams: NavParams) {
    this.menuCtrl.enable(true);
    this.user.email = this.aFAuth.auth.currentUser.email;
    if(this.aFAuth.auth.currentUser.displayName == null){
      this.user.name = "No display name available";
    }else{
      this.user.name = this.aFAuth.auth.currentUser.displayName;
    }
    if(this.aFAuth.auth.currentUser.photoURL == null){
      this.user.img = "";
    }else{
      this.user.img = this.aFAuth.auth.currentUser.photoURL;
      this.isPhotoAvailable = true;
    }
    this.user.password = "********";
  }

  editProfile(){
    this.navCtrl.push(ProfileEditPage);
  }

}
