import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from '../models/user';
import { ProfilePage } from '../profile/profile';
import { MenuController } from 'ionic-angular/components/app/menu-controller';



@Component({
  selector: 'page-profile-edit',
  templateUrl: 'profile-edit.html',
})
export class ProfileEditPage {

  user = {} as User;

  isPhotoAvailable = false;

  constructor(private menuCtrl: MenuController, private aFAuth: AngularFireAuth, public navCtrl: NavController, public navParams: NavParams) {
    this.menuCtrl.enable(true);
    this.user.email = this.aFAuth.auth.currentUser.email;
    if(this.aFAuth.auth.currentUser.displayName == null){
      this.user.name = "No name set yet";
    }else{
      this.user.name = this.aFAuth.auth.currentUser.displayName;
    }
    if(this.aFAuth.auth.currentUser.photoURL == null){
      this.user.img = "";
    }else{
      this.user.img = this.aFAuth.auth.currentUser.photoURL;
      this.isPhotoAvailable = true;
    }
  }


  resetPassword(){
    
  }

  saveChanges(){
    this.aFAuth.auth.currentUser.updateProfile({
      displayName: this.user.name,
      photoURL: this.aFAuth.auth.currentUser.photoURL
    }).then(_ => {
        this.navCtrl.push(ProfilePage);
    }).catch(err => {
      console.log("Update profile error: ", err);
    });
  }

}
