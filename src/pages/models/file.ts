export interface Record {
    uid: string;
    enzyme: string;
    base64: string;
    timestamp: Date;
}