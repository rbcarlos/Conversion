export interface User {
    email: string;
    password: string;
    img: string;
    name: string;
}