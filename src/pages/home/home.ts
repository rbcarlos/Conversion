import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams  } from 'ionic-angular';
import { File } from '@ionic-native/file';
import { Platform } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { SocialSharing } from '@ionic-native/social-sharing';
import { EmailComposer } from '@ionic-native/email-composer';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Record } from '../models/file';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  conversionValues: any = [
    [
        {
          name: "Ceralpha Units (CU/g)",
          value: 15.833
        },
        {
          name: "International Units (U/g)",
          value: 64.915          
        },
        {
          name: "SKB Units (SKB Units/g)",
          value: 5.993          
        },
        {
          name: "Dextrinizing Units (DU/g)",
          value: 4.252          
        },
        {
          name: "Farrand Units",
          value: 900.577          
        }
      ],
    [
      {
        name: "International Units (U/g)",
        value: 56.976 
      },
      {
        name: "Betamyl-3 Units (BU)",
        value: 1.421        
      },
      {
        name: "Betamyl Units",
        value: 83.262        
      }
    ],
    [
      {
        name: "International Units (U/g)",
        value: 51.116        
      },
      {
        name: "Betamyl-3 Units (BU)",
        value: 1.665        
      },
      {
        name: "Betamyl Units",
        value: 97.570        
      }
    ],
    [
      {
        name: "International Units (U/g)",
        value: 28.843        
      },
      {
        name: "Betamyl-3 Units (BU)",
        value: 1.421        
      },
      {
        name: "Betamyl Units",
        value: 83.262        
      }
    ],
    [
      {
        name: "International Units (U/g)",
        value: 0.064        
      },
      {
        name: "PullG6 Unit (PU)",
        value: 0.021        
      }
    ]
  ];
  enzymeNames = ["Alpha-amylase", "Beta-amylase (barley)", "Beta-amylase (barley malt)", "Beta-amylase (wheat)", "Limit dextrinase"];
  enzyme = "0";
  unitSelected = "0";
  value = 0;
  unitName = "Ceralpha Units (CU/g)";
  units = this.conversionValues[0];
  enzymeIndex : number = 0;
  unitIndex : number = 0;
  result:any = 0;
  exportString: string = "";
  output = "";

  files: Observable<{}[]>; // read collection

  record = {} as Record;

  constructor(private mail: EmailComposer, private plt: Platform, private frs: AngularFirestore, private social: SocialSharing,private menuCtrl: MenuController,private aFAuth: AngularFireAuth, private file: File, public navCtrl: NavController, public navParams: NavParams) {
    this.menuCtrl.enable(true);
  }

  ionViewDidLoad(){
    document.getElementById("menuToggle").hidden = false;
  }

  enzymeChanged(){
    this.units = this.conversionValues[parseInt(this.enzyme)];
    this.updateResult();
  }

  updateResult(){
    this.result = (this.value * this.conversionValues[parseInt(this.enzyme)][this.unitSelected].value / 5).toFixed(3);
    this.unitName = this.conversionValues[parseInt(this.enzyme)][this.unitSelected].name;
  }

  save(){
    this.exportString = "U/mL, ";
    this.conversionValues[this.enzymeIndex].forEach(element => {
      this.exportString += element.name + ", ";
    });
    this.exportString = this.exportString.slice(0, -3);
    this.exportString += "\n";
    this.exportString += this.value.toString() + ", ";
    this.conversionValues[this.enzymeIndex].forEach(element => {
      let result: string = ((element.value * this.value / 5).toFixed(3)).toString();
      this.exportString += result + ", ";
    });
    this.exportString = this.exportString.slice(0, -3);
    console.log(this.exportString);
    console.log(this.aFAuth.auth.currentUser.email);
    this.file.writeFile(this.file.dataDirectory, "export.csv", this.exportString, {replace: true})
        .then(fileresult => {
          console.log(fileresult);
          this.file.readAsDataURL(this.file.dataDirectory, "export.csv")
          .then(result => {
            this.record.uid = this.aFAuth.auth.currentUser.uid;
            this.record.timestamp = new Date();
            this.record.base64 = result.replace('comma-separated-values', 'csv');
            this.record.enzyme = this.enzymeNames[this.enzyme];
            this.frs.collection("records").add(this.record);
          }).catch(err => console.log(err));
        })
        .catch(err => console.log(err));
  }


  send(){
    this.exportString = "U/mL, ";
    this.conversionValues[this.enzymeIndex].forEach(element => {
      this.exportString += element.name + ", ";
    });
    this.exportString = this.exportString.slice(0, -3);
    this.exportString += "\n";
    this.exportString += this.value.toString() + ", ";
    this.conversionValues[this.enzymeIndex].forEach(element => {
      let result: string = ((element.value * this.value / 5).toFixed(3)).toString();
      this.exportString += result + ", ";
    });
    this.exportString = this.exportString.slice(0, -3);
    console.log(this.exportString);
    console.log(this.aFAuth.auth.currentUser.email);
    this.file.writeFile(this.file.dataDirectory, "export.csv", this.exportString, {replace: true})
        .then(fileresult => {
          console.log(fileresult);
          this.file.readAsDataURL(this.file.dataDirectory, "export.csv")
          .then(result => {
            this.output = result;
            result = result.replace('comma-separated-values', 'csv');
            let message: string = "This is your conversion file for " + this.enzymeNames[this.enzyme] + " enzyme. You can use this file further in Excel.";
            let subject: string = "GlycoSpot Unit Conversion";
            let recipient: string[] = [this.aFAuth.auth.currentUser.email];
            let email = {
              to: 'max@mustermann.de',
              attachments: [ result ],
              subject: subject,
              body: message,
              isHtml: true
            }
            if(this.plt.is("android")){
              this.social.shareViaEmail(message, subject, recipient, null, null, result)
            .then(_ => console.log("successfully shared."))
            .catch(err => console.log(err));
            }else if(this.plt.is("ios")){
              this.mail.open(email)
              .then(_ => console.log("email sent"))
              .catch(err => console.log(err));
            }
          });
        })
        .catch(err => console.log("error: ",err));
  }
}
