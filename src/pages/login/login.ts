import { Component } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { User } from '../models/user';
import { AngularFireAuth } from 'angularfire2/auth';
import { HomePage } from '../home/home';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  user = {} as User;

  loginErr = false;

  emailControl;
  passwordControl;

  


  constructor(private menuCtrl: MenuController, private aFAuth: AngularFireAuth, public navCtrl: NavController, public navParams: NavParams) {
    this.menuCtrl.enable(false);
  }

  async login(user: User){
    try {
      const result = await this.aFAuth.auth.signInWithEmailAndPassword(user.email, user.password);
      if(result){
        this.menuCtrl.enable(true);
        this.navCtrl.push(HomePage);
      }
    } catch (error) {
      this.loginErr = true;
    }
  }

  register(){
    this.navCtrl.push(RegisterPage);
  }

}
